class Coffee {
  final int protien;
  final int caffiene;
  final int calories;
  final String description;
  final String name;
  final double rate;
  final String shopName;
  bool isFavorite;

  Coffee(
    this.name,
    this.shopName,
    this.isFavorite,
    this.rate,
    this.description,
    this.calories,
    this.protien,
    this.caffiene,
  );
}
